public abstract class AbstractAudio extends AbstractMedia implements Audio {
    private int durata,volume;

    public AbstractAudio(String titolo, int durata) {
        super(titolo);
        this.durata = durata;
        this.volume = 5;
    }

    @Override
    public void louder() {
        volume++;
    }

    @Override
    public void weaker() {
        volume--;
    }

    @Override
    public String play() {
        return " Titolo "+getTitolo() + " Volume= "+ Integer.toString(volume) +" Durata= "+ Integer.toString(durata);
    }

    public int getDurata() {
        return durata;
    }

    public int getVolume() {
        return volume;
    }
}
