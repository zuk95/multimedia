public abstract class AbstractMedia implements Media {
    private String titolo;

    public AbstractMedia(String titolo) {
        this.titolo = titolo;
    }

    @Override
    public String getTitolo() {
        return titolo;
    }
}
