public abstract class AbstractVideo extends AbstractMedia implements Video{
    private int luminosita;

    public AbstractVideo(String titolo) {
        super(titolo);
        this.luminosita = 5;
    }

    @Override
    public void brighter() {
        luminosita++;
    }

    @Override
    public void darkness() {
        luminosita--;
    }

    @Override
    public String play() {
        return "Titolo= "+getTitolo() + " Luminosita= "+ Integer.toString(luminosita);
    }

    public int getLuminosita() {
        return luminosita;
    }
}
