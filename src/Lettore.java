public interface Lettore {
    boolean addElemento(Media e);
    boolean rmvElemento(String titolo);
    String execute(String titolo);
    String toString();
    void piuVol();
    void menoVol();
    void piuLum();
    void menoLum();
    String visualizza();
}
