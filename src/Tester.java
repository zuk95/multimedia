public class Tester {
    public static void main(String[] args) {
        Media riprod = new TracciaAudio("Primo Audio",5);
        Media pic = new Immagine("Prima Immagine");
        TracciaAudio filmAudio = new TracciaAudio("null",10);
        Immagine video = new Immagine("video");
        Media film = new Filmato("Primo Filmato",filmAudio,video);

        System.out.println(riprod.play());
        System.out.println(pic.play());
        System.out.println(film.play());

        Lettore samsung = new LettoreMultimediale();

        samsung.addElemento(riprod);
        samsung.addElemento(pic);
        samsung.addElemento(film);

        System.out.println(samsung);

        samsung.rmvElemento("Prima Immagine");

        System.out.println(samsung);

        System.out.println(samsung.execute("Primo Filmato"));

        samsung.piuVol();

        System.out.println(samsung.visualizza());

        samsung.piuLum();
        samsung.piuLum();

        System.out.println(samsung.visualizza());

        samsung.execute("Primo Audio");

        System.out.println(samsung.visualizza());



    }
}
