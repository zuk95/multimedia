public class Filmato extends AbstractMedia implements Audio,Video{
    private TracciaAudio sound;
    private Immagine view;

    public Filmato(String titolo, TracciaAudio sound, Immagine view) {
        super(titolo);
        this.sound = sound;
        this.view = view;
    }

    @Override
    public void louder() {
        sound.louder();
    }

    @Override
    public void weaker() {
        sound.weaker();
    }

    @Override
    public void brighter() {
        view.brighter();
    }

    @Override
    public void darkness() {
        view.darkness();
    }

    @Override
    public String play() {
        return " Titolo "+getTitolo() + " Volume= "+ Integer.toString(sound.getVolume()) +
                " Durata= "+ Integer.toString(sound.getDurata())+
                " Luminosita= "+ Integer.toString(view.getLuminosita());
    }
}
