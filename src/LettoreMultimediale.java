import java.util.ArrayList;

public class LettoreMultimediale implements Lettore {
    private ArrayList<Media> lettore;
    Media inRiproduzione;

    public LettoreMultimediale() {
        this.lettore = new ArrayList<>();
        inRiproduzione=null;
    }

    @Override
    public boolean addElemento(Media e) {
        return lettore.add(e);
    }

    @Override
    public boolean rmvElemento(String titolo) {
        for (Media e: lettore
             ) {
            if(e.getTitolo()==titolo){
                return lettore.remove(e);
            }
        }
        return false;
    }

    @Override
    public String execute(String titolo) {
        for (Media e:lettore
             ) {
            if(e.getTitolo()==titolo){
                inRiproduzione=e;
                return inRiproduzione.play();
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "LettoreMultimediale{" +
                "lettore=" + lettore +
                '}';
    }

    @Override
    public void piuVol() {
        if(inRiproduzione instanceof Audio){
            ((Audio) inRiproduzione).louder();
        }
    }

    @Override
    public void menoVol() {
        if(inRiproduzione instanceof Audio){
            ((Audio) inRiproduzione).weaker();
        }
    }

    @Override
    public void piuLum() {
        if(inRiproduzione instanceof Video){
            ((Video) inRiproduzione).brighter();
        }
    }

    @Override
    public void menoLum() {
        if(inRiproduzione instanceof Video){
            ((Video) inRiproduzione).darkness();
        }
    }

    public String visualizza(){
         return  inRiproduzione.play();
    }
}
