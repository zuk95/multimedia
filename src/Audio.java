public interface Audio extends Media {
    void louder();
    void weaker();
}
